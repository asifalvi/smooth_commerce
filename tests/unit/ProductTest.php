<?php

namespace App\Tests;

use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Validator\Validation;
use Symfony\Component\HttpFoundation\Response;

/**
 * Product Testing Class
 * @author Damco Developer
 * @version 1.0
 */
class ProductTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;
    protected $em;

    protected function _before()
    {
        $this->em = $this->getModule('Doctrine2')->em;
    }

    /**
     * Test is getting all the products
     */
    public function testIsGettingAllTheProduct()
    {
        $products = $this->em->getRepository(Product::class)->findAll();
        $product = $products[0];
        $this->assertEquals('Product Updated using codeception api testing', $product->getName());
    }

    /**
     * Test Entity All Attributes Set
     */
    public function testIsEntityAllAttributeSet()
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();
        $product = new Product();
        $product->setName('Product from unit testing');
        $product->setSku('JD9D8');
        $product->setPrice(14.5);
        $violations = $validator->validate($product);
        $this->assertTrue(count($violations) == 0);
    }

    /**
     * Test Entity All Attributes Not Set
     */
    public function testIsEntityAllAttributeNotSetOrEmpty()
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();
        $product = new Product();
        $product->setName('Product from unit testing');
        $product->setSku('');
        $product->setPrice(14.5);
        $violations = $validator->validate($product);
        $this->assertFalse(count($violations) == 0);
    }

    /**
     * Test Is Saving A Single Product
     */
    public function testIsSavingASingleProduct()
    {
        $product = new Product();
        $product->setName('Product from unit testing');
        $product->setSku('JD9D8');
        $product->setPrice(14.5);
        $categories = [1];
        foreach ($categories as $catId) {
            $category = $this->em->getRepository(Category::class)->findOneBy(['id' => $catId]);
            $product->addCategory($category);
        }
        $this->em->persist($product);
        $this->em->flush();
        $this->assertEquals('Product from unit testing', $product->getName());
    }

    /**
     * Test Is Getting A Single Product
     */
    public function testIsGettingASingleProduct()
    {
        $product = $this->em->find(Product::class, 4);
        $this->tester->assertEquals('Product using codeception api testing', $product->getName());
    }

    /**
     * Test Updating a Single Product
     */
    public function testIsUpdatingASingleProduct()
    {
        $product = $this->em->find(Product::class, 12);
        $product->setName('Product Updating from unit testing');
        $product->setSku('JD9D8');
        $product->setPrice(14.5);
        $categories = [1];
        foreach ($categories as $catId) {
            $category = $this->em->getRepository(Category::class)->findOneBy(['id' => $catId]);
            $product->addCategory($category);
        }
        $this->em->persist($product);
        $this->em->flush();
        $this->assertEquals('Product Updating from unit testing', $product->getName());
    }

    /**
     * Test Is Deleting A Single Product
     */
    public function testIsDeletingASingleProduct()
    {
        $product = $this->em->find(Product::class, 12);
        $this->em->remove($product);
        $this->em->flush();
        $this->tester->assertEquals('Product using codeception api testing', $product->getName());
    }
}
