<?php

namespace App\Tests;

use App\Entity\Category;

/**
 * Category Testing Class
 * @author Damco Developer
 * @version 1.0
 */
class CategoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;
    protected $em;

    protected function _before()
    {
        $this->em = $this->getModule('Doctrine2')->em;
    }

    /**
     * Test Is Getting All The Categories
     */
    public function testIsGettingAllTheCategories()
    {
        $categories = $this->em->getRepository(Category::class)->findAll();
        $category = $categories[0];
        $this->assertEquals('Category One', $category->getName());
    }
}
