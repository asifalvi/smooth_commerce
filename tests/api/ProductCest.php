<?php

namespace App\Tests;

use App\Tests\ApiTester;

/**
 * Product Testing Class
 * @author Damco Developer
 * @version 1.0
 */
class ProductCest
{

    /**
     * Testing Is Getting All the Product
     * @param ApiTester
     */
    public function testIsGettingAllTheProducts(ApiTester $apiTester)
    {
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendGet('/product');
        $apiTester->seeResponseCodeIs(200);
        $apiTester->seeResponseIsJson();
        $apiTester->seeResponseContains('id');
    }

    /**
     * Test for saving one product
     * @param ApiTester
     */
    public function testIsAddingOneProduct(ApiTester $apiTester)
    {
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendPost('/product/add', json_encode([
            'name' => 'Product using codeception api testing',
            'sku' => 'KD8DK',
            'price' => 13.3,
            'categories' => [1, 2]
        ]));
        $apiTester->seeResponseCodeIs(201);
        $apiTester->seeResponseIsJson();
        $apiTester->seeResponseContains('Product using codeception api testing');
    }

    /**
     * Testing is getting a single product
     * @param ApiTester
     */
    public function testIsGettingASingleProduct(ApiTester $apiTester)
    {
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendGet('/product/', ['id' => 1]);
        $apiTester->seeResponseCodeIs(200);
        $apiTester->seeResponseIsJson();
        $apiTester->seeResponseContainsJson(['id' => 1]);
    }

    /**
     * Testing is updating a single product
     * @param ApiTester
     */
    public function testIsUpdatingASingleProduct(ApiTester $apiTester)
    {
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendPut('/product/1', json_encode([
            'name' => 'Product Updated using codeception api testing',
            'sku' => 'KD8DK',
            'price' => 13.3,
            'categories' => [1, 2]
        ]));
        $apiTester->seeResponseCodeIs(200);
        $apiTester->seeResponseIsJson();
        $apiTester->seeResponseContainsJson(['id' => 1]);
    }

    /**
     * Testing Is Deleting a Single Product
     * @param ApiTester
     */
    public function testIsDeletingASingleProduct(ApiTester $apiTester)
    {
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendDelete('/product/6');
        $apiTester->seeResponseCodeIs(200);
        $apiTester->seeResponseIsJson();
        $apiTester->seeResponseContainsJson([]);
    }
}
