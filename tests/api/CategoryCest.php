<?php

namespace App\Tests;

use App\Tests\ApiTester;

/**
 * Category Testing Class
 * @author Damco Developer
 * @version 1.0
 */
class CategoryCest
{
    /**
     * Testing is getting all categories
     * @param ApiTester
     */
    public function testIsGettingAllCategories(ApiTester $apiTester)
    {
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendGet('/category/');
        $apiTester->seeResponseCodeIs(200);
        $apiTester->seeResponseIsJson();
        $apiTester->seeResponseContains('id');
    }
}
