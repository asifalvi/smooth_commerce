<?php

namespace App\Service;

/**
 * Validation Service to validation product entity
 * @author Damco Developer
 * @version 1.0
 */
class ValidationService
{
    /**
     * Validation for Product Entity
     * @param Array
     * @return Array
     */
    public function validate(array $data)
    {
        $errors = [];
        if (!isset($data['name']) or empty($data['name'])) {
            $errors['name'] = 'Name field is invalid';
        }
        if (!isset($data['sku']) or empty($data['sku'])) {
            $errors['sku'] = 'SKU field is invalid';
        }
        if (!isset($data['price']) or empty($data['price'])) {
            $errors['price'] = 'Price field is invalid';
        }
        if (!isset($data['categories']) or empty($data['categories'])) {
            $errors['categories'] = 'Categories field is invalid';
        }
        return $errors;
    }
}
