<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Product Service to handle all the operation for products
 * @author Damco Developer
 * @version 1.0
 */
class ProductService
{
    private $entityManagerInterface, $productRepository, $categoryRepository;
    public function __construct(
        EntityManagerInterface $entityManagerInterface,
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Get All Products
     * @return Product
     */
    public function getAllProducts()
    {
        return $this->productRepository->findAll();
    }

    /**
     * Saving a single product
     * @param Array
     * @return Product
     */
    public function storeProduct(array $data)
    {
        $product = new Product();
        $product->setName($data['name']);
        $product->setSku($data['sku']);
        $product->setPrice($data['price']);

        $categories = $data['categories'];
        foreach ($categories as $catId) {
            $category = $this->categoryRepository->findOneBy(['id' => $catId]);
            $product->addCategory($category);
        }

        $this->entityManagerInterface->persist($product);
        $this->entityManagerInterface->flush();
        return $product;
    }

    /**
     * Getting a single product
     * @param Int
     * @return Product
     */
    public function getSingleProductById(int $id)
    {
        return $this->productRepository->findOneBy(['id' => $id]);
    }

    /**
     * Updating a single product
     * @param Array
     * @param Int
     * @return Product
     */
    public function updateSingleProduct(array $data, int $id)
    {
        $product = $this->productRepository->findOneBy(['id' => $id]);
        if (empty($product)) {
            return false;
        }
        empty($data['name']) ? true : $product->setName($data['name']);
        empty($data['sku']) ? true : $product->setSku($data['sku']);
        empty($data['price']) ? true : $product->setPrice($data['price']);

        if (!empty($data['categories'])) {
            $categories = $data['categories'];
            foreach ($categories as $catId) {
                $category = $this->categoryRepository->findOneBy(['id' => $catId]);
                $product->addCategory($category);
            }
        }

        $this->entityManagerInterface->persist($product);
        $this->entityManagerInterface->flush();
        return $product;
    }

    /**
     * Deleting a single product
     * @param Int
     * @return Product
     */
    public function deleteSingleProduct(int $id)
    {
        $product = $this->productRepository->findOneBy(['id' => $id]);
        if (empty($product)) {
            return false;
        }
        $this->entityManagerInterface->remove($product);
        $this->entityManagerInterface->flush();
        return true;
    }
}
