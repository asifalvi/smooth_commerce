<?php

namespace App\Controller;

use FOS\RestBundle\View\View;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;

/**
 * Category Controller class to manange all category operations
 * @author Damco Developer
 * @version 1.0
 */
class CategoryController extends AbstractFOSRestController
{
    private $categoryRepository;
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Getting all the category list
     * @return Json
     * @Rest\Get("/api/category", name="category")
     */
    public function index(): View
    {
        return View::create($this->categoryRepository->findAll(), Response::HTTP_OK);
    }
}
