<?php

namespace App\Controller;

use FOS\RestBundle\View\View;
use App\Service\ProductService;
use App\Service\ValidationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;

/**
 * Product Controller class to manange all product operations
 * @author Damco Developer
 * @version 1.0
 * @Route("api/product", name="product.")
 */
class ProductController extends AbstractFOSRestController
{
    private $productService, $validationService;
    public function __construct(ProductService $productService, ValidationService $validationService)
    {
        $this->productService = $productService;
        $this->validationService = $validationService;
    }

    /**
     * Getting all the products
     * @Rest\Get("/", name="all")
     * @return Json 
     */
    public function index(): View
    {
        return View::create($this->productService->getAllProducts(), Response::HTTP_OK);
    }

    /**
     * Adding a single product
     * @param Request
     * @return Json
     * @Rest\Post("/add", name="add")
     */
    public function addProduct(Request $request): View
    {
        if ($request->getContentType() != 'json') {
            return View::create(['Only json input is allowed'], Response::HTTP_BAD_GATEWAY);
        }
        $data = $this->arrayToJson($request);
        $violations = $this->validationService->validate($data);
        if (!empty($violations)) {
            return View::create($violations, Response::HTTP_BAD_REQUEST);
        }
        $product = $this->productService->storeProduct($data);
        return View::create($product, Response::HTTP_CREATED);
    }

    /**
     * Getting a single product
     * @param Int $id
     * @return Json
     * @Rest\Get("/{id}", name="single")
     */
    public function getProduct(int $id): View
    {
        return View::create($this->productService->getSingleProductById($id), Response::HTTP_OK);
    }

    /**
     * Updating a single product
     * @param Request
     * @param Int 
     * @return Json
     * @Rest\Put("/{id}", name="update")
     */
    public function updateProduct(Request $request, int $id): View
    {
        if ($request->getContentType() != 'json') {
            return View::create(['Only json input is allowed'], Response::HTTP_BAD_GATEWAY);
        }
        $data = $this->arrayToJson($request);
        $result = $this->productService->updateSingleProduct($data, $id);
        if (!$result) {
            return View::create(['Product Not Found'], Response::HTTP_BAD_GATEWAY);
        }
        return View::create($result, Response::HTTP_OK);
    }

    /**
     * Deleting a Single product
     * @param Int 
     * @return Json
     * @Rest\Delete("/{id}", name="delete")
     */
    public function deleteProduct(int $id): View
    {
        $result = $this->productService->deleteSingleProduct($id);
        if (!$result) {
            return View::create(['Product Not Found'], Response::HTTP_BAD_GATEWAY);
        }
        return View::create([], Response::HTTP_OK);
    }

    /**
     * Convert Request to Json
     * @param Request
     * @return Array
     */
    public function arrayToJson($request): array
    {
        return json_decode($request->getContent(), true);
    }
}
