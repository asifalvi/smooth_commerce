Smooth Commerce - Spicy Deli
==================================

This Symonfy Application consists of RESTful API, API Tests and Unit Tests. The application has been developed considering the requirements of Spicy Deli's outlines for the project. The APIs will enable Spicy Deli to build mobile and desktop/web clients in future to enhance their online presence.


## PreRequistes

* PHP 7.*
* MySQL 5.7.*
* Git
* Composer


## Installation


### Application Setup

**Clone the repo**:
```bash
$ git clone https://asifalvi@bitbucket.org/asifalvi/smooth_commerce.git
```

**Change directory**:
```bash
$ cd smooth_commerce
```

**Install dependencies with composer**:
```bash
$ composer install
```
**Database Connection**:

Change the database connection in .env file and provide replace db_user, db_password and db_name with actual values.
```bash
$ DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
```

### Database Setup

**Create Database**:
```bash
php bin/console doctrine:database:create
```

**Create Schema**:
```bash
php bin/console doctrine:schema:update -f
```

### Server Setup
Setup a compatible web server (i.e. Apache, Nginx) with root directory targeting to Public directory of the application.

or

Start a local web server
```bash
$ symfony server:start
```

## API Endpoints

| Method        | URL           | Description  |
| ------------- |:-------------| :-----|
| **POST**      | /api/add/ | Create a product |
| **GET**       | /api/product/ | List all products |
| **GET**       | /api/product/:id | Retrieve a single product |
| **PUT**       | /api/product/:id | Update one product |
| **DELETE**    | /api/product/:id  | Delete a product |
| **GET**       | /api/category/ | List all product categories |

## Payload
```bash
 {
    "name" : "Product One",
    "sku" : "90D9D",
    "price" : 12.20,
    "categories" : [1,2]
 }
```


## Source Code

* The developer generated code resides in the following files: 
	* src/Controller/ProductController 
	* src/Controller/CategoryController 
	* src/Service/ProductService
	* src/Service/ValidationService
	* /tests/api/CategoryCest.php
	* /tests/api/ProductCest.php
	* /tests/unit/ProductTest.php
	* /tests/unit/CategoryTest.php

##  API Testing Using Codeception

```bash
$ php vendor/bin/codecept run api
```

Unit Testing Using Codeception
---------------------------

```bash
$ php vendor/bin/codecept run unit
```